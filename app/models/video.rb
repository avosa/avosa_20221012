class Video < ApplicationRecord
  belongs_to :user, class_name: 'User'

  validates :title, presence: true
  validates :category, presence: true
  validate :thumbnail_type
  validate :video_type
  validate :video_size
  validates_presence_of :video, message: "video can't be blank."

  has_one_attached :video
  has_one_attached :thumbnail

  enum category: { exercise: 0, education: 1, recipe: 2 }

  private

  def thumbnail_type
    return unless thumbnail.attached? && !thumbnail.attachment.blob.content_type.in?(%w[image/jpg image/jpeg image/png])

    errors.add(:thumbnail, 'must be an image file.')
  end

  def video_size
    return unless video.attached? && video.byte_size > 200.megabytes

    errors.add(:video, 'should be less than 200MB.')
  end

  def video_type
    return unless video.attached? && !video.content_type.in?(%w[video/mp4 video/mov])

    errors.add(:video, ' must be in MP4 or MOV format.')
  end
end
