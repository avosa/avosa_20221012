class VideoSerializer
  include JSONAPI::Serializer
  attributes :title, :category, :video_url, :img_64, :img_128, :img_256
end
