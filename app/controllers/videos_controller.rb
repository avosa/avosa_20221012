class VideosController < ApplicationController # rubocop:disable Style/Documentation
  before_action :authorize_request, except: %i[index show]
  def index
    videos = Video.all
    render json: VideoSerializer.new(videos), status: 200
    # render json: { video_details: @videos }
  end

  def show
    @video = Video.find(params[:id])
    render json: { video_url: @video['video_url'], video: @video }
  end

  def create # rubocop:disable Metrics/MethodLength
    upload_thumbnails
    upload_vodeos
    @video = @current_user.videos.create(video_params)

    if @video.save

      set_vedio_meta

      save_video_urls
      save_thumb_name

      save_images

      render json: { status: 201, video: @video_url, location: @video }
    else
      render json: { message: @video.errors, status: :unprocessable_entity }
    end
  end

  private

  def video_params
    params.permit(:title, :category, :video, :thumbnail)
  end

  def upload_thumbnails # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    @resized_images = {
      img_64: {
        transformation: [
          { width: 64, height: 64, crop: :limit, folder: 'images' }
        ]
      },
      img_128: {
        transformation: [
          { width: 128, height: 128, crop: :limit, folder: 'images' }
        ]
      },
      img_256: {
        transformation: [
          { width: 256, height: 256, crop: :limit, folder: 'images' }
        ]
      }
    }
    @image_64 = Cloudinary::Uploader.upload(video_params['thumbnail'], folder: 'images',
                                                                       transformation: @resized_images[:img_64][:transformation])
    @image_128 = Cloudinary::Uploader.upload(File.open(video_params['thumbnail']), folder: 'images',
                                                                                   transformation: @resized_images[:img_128][:transformation])
    @image_256 = Cloudinary::Uploader.upload(File.open(video_params['thumbnail']), folder: 'images',
                                                                                   transformation: @resized_images[:img_256][:transformation])
  end

  def save_images
    @video.update(img_64: @image_64['secure_url'])
    @video.update(img_128: @image_128['secure_url'])
    @video.update(img_256: @image_256['secure_url'])
  end

  def save_video_urls
    @video.update(video_url: @video_url)
  end

  def save_thumb_name
    @video.update(thumbnail_name: @video.thumbnail.filename)
  end

  def upload_vodeos
    @blob_video = Cloudinary::Uploader.upload_large(video_params['video'], resource_type: 'video', folder: 'videos',
                                                                           chunk_size: 6_000_000)
  end

  def set_vedio_meta
    @video_cloudinary = @blob_video
    @video_url = @video_cloudinary['secure_url']
  end
end
