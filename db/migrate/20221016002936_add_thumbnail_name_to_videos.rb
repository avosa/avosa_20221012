class AddThumbnailNameToVideos < ActiveRecord::Migration[7.0]
  def change
    add_column :videos, :thumbnail_name, :string
  end
end
