class CreateVideos < ActiveRecord::Migration[7.0]
  def change
    create_table :videos do |t|
      t.string :title
      t.integer :category
      t.string :img_64
      t.string :img_128
      t.string :img_256

      t.timestamps
    end
  end
end
