Rails.application.routes.draw do
  root to: 'users#welcome'

  # User registration, login and email verification routes
  resources :users, only: [:create]
  post '/users/login', to: 'users#login'
  get '/users/verify_email/:token', to: 'users#verify_email'

  # Videos routes
  resources :videos, only: %i[create index show]
end
