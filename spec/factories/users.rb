FactoryBot.define do
  factory :user do
    name { "MyString" }
    email { "MyString" }
    password_digest { "MyString" }
    confirm_token { "MyString" }
    email_confirmed { false }
  end
end
