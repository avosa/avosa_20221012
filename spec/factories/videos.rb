FactoryBot.define do
  factory :video do
    title { "MyString" }
    category { 1 }
    img_64 { "MyString" }
    img_128 { "MyString" }
    img_256 { "MyString" }
  end
end
