# Video Upload Server

This a RESTful Api for a secure file upload. The frontend/UI of the same is available [here, as avosa_ui.](https://gitlab.com/avosa/avosa_ui)

The backend/api has been hosted on heroku and is live [here](https://api-my-hosp.herokuapp.com/).

The frontend has been hosted on Vercel and is live [here](https://video-upload-gray.vercel.app/).

# Getting started.
To start testing this microservice, you need to have [Ruby 3.1.2](https://www.ruby-lang.org/en/downloads/), [PostgresQL](https://www.postgresql.org/) and [Rails 7.0.4](https://rubygems.org/gems/rails/versions/7.0.4) installed on your Mac/PC.

For testing purposes, you need either [Postman](https://www.postman.com/), Visual Studio Code Extension like [Thunder Client](https://www.thunderclient.com/) or a similar extension installed as well. You may also opt to use [Curl](https://curl.se/) if you are a CLI geek! 😎

Easiest way to get started is by running:
```
rails db:create && rails db:migrate && rails s
```

**IMPORTANT:** To test using docker follow the steps below:

1. Set up database
```
mv config/database.yml config/database.rails-default.yml
mv config/database.docker.yml config/database.yml
```
2. Run `docker-compose up` or just `docker compose up` if using the latest version of docker 

3. Create database
```
docker-compose run --rm api bin/rails db:create 
```

4. Run migrations
```
docker-compose run --rm api bin/rails db:migrate 
```

Whichever way you choose to start the API Server, it will be Listening on [http://localhost:3000](http://127.0.0.1:3000).


# Endpoints
# 1. Register - POST /users
```
curl --location --request POST 'http://localhost:3000/users' \
--form 'name="John Dore"' \
--form 'email="johndore@gmail.com"' \
--form 'password="123456789"' \
--form 'password_confirmation="123456789"' \
--form 'profile_picture=@"/path/to/file"' 
```

# 2. Login - POST /users/login
```
curl --location --request POST 'http://localhost:3000/users/login' \
--data-urlencode 'email=johndore@gmail.com' \
--data-urlencode 'password=123456789'
```

# 3. POST /videos
```
curl --location --request POST 'http://localhost:3000/videos' \
--form 'title="Yoga"' \
--form 'category="exercise"' \
--form 'video="@"/path/to/video_file"' \
--form 'thumbnail="@"/path/to/file"' 
```

# 4.  GET /videos
```
curl --location --request GET 'http://localhost:3000/videos'
```

# 5. GET /videos/video_id
```
curl --location --request GET 'http://localhost:3000/videos/1'

```

Full documentaion is available [here](https://www.postman.com/webster-codes/workspace/task/collection/12679708-8334f6c3-c0f6-4628-a53d-e95785dd6388?action=share&creator=12679708)

**Some reminder:** The frontend to this api is available [here, as avosa_ui.](https://gitlab.com/avosa/avosa_ui)

Enjoy!

# Author

[Webster Avosa](https://github.com/avosa)
