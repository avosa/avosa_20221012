FROM ruby:3.1.2

RUN apt-get update -yqq && apt-get install -yqq --no-install-recommends \
    nodejs

COPY Gemfile* /usr/src/app/

WORKDIR /usr/src/app

ENV BUNDLE_PATH /gems​

RUN bundle install 

COPY docker-entrypoint.sh /usr/src/app/

COPY . /usr/src/app/​

RUN ["chmod", "+x", "/usr/src/app/docker-entrypoint.sh"]

CMD ["bin/rails", "s", "-b", "0.0.0.0"]​
